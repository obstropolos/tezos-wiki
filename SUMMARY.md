# Summary

* [Introduction to the Tezos Wiki](README.md)
* [Tezos Whitepaper](files/whitepaper.md)

### Tezos Basics

* [What is Tezos?](files/basics.md#intro)
* [What makes Tezos unique?](files/basics.md#unique)
* [Tezos Nomenclature](files/basics.md#nomenclature)

### Proof-of-Stake
* [What is Proof-of-Stake?](files/proofofstake.md#intro)
* [The Tezos consensus algorithm](files/proofofstake.md#consensus)

### [Baking](files/baking.md)
* [What is baking?](files/baking.md#what)
* [What is delegating?](files/baking.md#delegate)
* [Should I bake or delegate?](files/baking.md#bakeordelegate)

### [Tezos Governance](files/self-amendment.md)
* [What is Self Amendment?](files/self-amendment.md#introduction)
* [Off-chain Governance](files/self-amendment.md#offchain)

### [Smart Contracts](files/language.md)
* [Michelson](files/language.md#michelson)
* [SmartPy](files/language.md#smartpy)
* [LIGO](files/language.md#ligo)

### [Formal Verification](files/formal-verification.md)
* [Michelson and Coq](files/formal-verification.md#coq)

### [Future Developments](files/future.md)
* [Privacy](files/future.md#intro)
* [Consensus](files/future.md#consensus)
* [Layer 2](files/future.md#layer2)
* [Governance](files/future.md#governance)

### Built on Tezos
* [Key Tezos Projects](files/projects.md#projects)

### Resources
* [Wallets](files/resources.md#wallet)
* [Block Explorer](files/resources.md#explorer)
* [Baking Services](files/resources.md#baking)
